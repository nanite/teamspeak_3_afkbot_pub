module.exports = {
    server: 'SERVER',
    auth: {
        username: 'QUERY-USERNAME',
        password: 'QUERY-PASSWORD'
    },
    msgToClient: 'Der Afk-Bot hat dich erwischt ;P',
    ideleTimeInMin: 20,
    whitelistGroup: [ 6, 9 ], // 6=admin, 9=co-admin
    afkCannelId: 17,
    whitelistChannels: [
        16, // Redlight
        28, // Kuschelecke
    ]
};