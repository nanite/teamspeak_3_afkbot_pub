var TeamSpeakClient = require("node-teamspeak"),
    _ = require('underscore');

var config = require('./etc/config');
var badClients = [];
	config.whitelistChannels.push( config.afkCannelId  );


var cl = new TeamSpeakClient(config.server);
cl.send( 'login', { client_login_name: config.auth.username, client_login_password: config.auth.password }, function( err, response, rawResponse ){
    cl.send( 'use', { sid: 1 }, function( err, response, rawResponse ){
		afkCheck();
    });
});

function afkCheck(){
	cl.send( 'clientlist', [ 'voice', 'groups', 'channel' ], function( err, response, rawResponse ){
            var tmpBadClients = _.filter( response, function( client ){
					        		return ( 
							        			//client.client_output_hardware === 0 || 
							        			client.client_input_hardware === 0 || 
							        			client.client_input_muted === 1 || 
							        			client.client_output_muted === 1 || 
							        			client.client_away === 1 
						        			) && 
						        			_.indexOf( config.whitelistChannels, client.cid ) === -1 &&
						        			_.indexOf( config.whitelistGroup,  client.client_servergroups ) === -1
                          && client.client_database_id !== 1
						        			//&& client.clid === 10
					            });

			var twentyMinutesLater = new Date();
			twentyMinutesLater.setMinutes( twentyMinutesLater.getMinutes() + config.ideleTimeInMin );

			// drop non afkler from old list
			var tmpList = _.filter( badClients, function( badClient ){
				var tmpArray = _.find( tmpBadClients, function( client ){
					return client.cid === badClient.cid;
				});
				return tmpArray;
			});
			badClients = tmpList;

			_.each( tmpBadClients, function( client ){
				var stillExist = _.find( badClients, function( badClient ){
					return badClient.clid === client.clid;
				});

				if( !stillExist ){
					client.moveDate = twentyMinutesLater;
					badClients.push( client );
				} else {
					// check if max idletime
					if( +(new Date) > +stillExist.moveDate ){
						console.log( 'move him' );

						badClients = _.without( badClients, stillExist );

						// move client
            cl.send( 'clientmove', { clid: stillExist.clid, cid: config.afkCannelId }, function( err, response, rawResponse ){
        			if( err )
        				console.log( err );

              console.log( 'afkler moved: ' + stillExist.client_nickname );    			
              cl.send( 'sendtextmessage', { targetmode: 1, target: stillExist.clid, msg: config.msgToClient }, function( err, response, rawResponse ){
        				if( err )
        					console.log( err );
        			});
          	});
					} else {
						/*console.log( 'wait for moving' );
						console.log( new Date() );
						console.log( stillExist.moveDate );*/
					}
				}
			});
		setTimeout(function(){
			afkCheck();
		}, 5000 );
    });
}